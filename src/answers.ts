// task 1:
// the original code executes async functions one by one
// since processOrder doesn't depend on the outcome of updateAnalytics call
// the calls can be combined under Promise.all
//
// async function completeCheckout(orderId) {
//         const [_, order] = await Promise.all([
//                 updateAnalytics(orderId),
//                 processOrder(orderId)
//         ])
//
//         return order;
// }

// task 2:
// the catch block will not catch the error
// setTimeout is not executed synchroniously
// so it will be executed long after try/catch block is finished

// task 3:
// the problem is with arr[i] assertion. If arr[i] == 0
// it will be resolved to false skipping the calculation
// which will result in unaccurate result
// p.s. some of the candidates rumbles that you cannot divide by 0.
// common, this is JS - you can divide by 0. It will result in Infinite

// task 4:
// "Welcome  John" <- note double space
// "Hello Mr. John"
