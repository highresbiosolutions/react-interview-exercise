import * as React from "react";

function Name() {
  const [val, setVal] = React.useState("");
  return (
    <div>
      <label htmlFor="name">Name: </label>
      <input
        id="name"
        value={val}
        onChange={(event) => setVal(event.target.value)}
      />
    </div>
  );
}

function FavoriteAnimals() {
  const [val, setVal] = React.useState("");
  return (
    <div>
      <label htmlFor="animal">Favorite Animal: </label>
      <input
        id="animal"
        value={val}
        onChange={(event) => setVal(event.target.value)}
      />
    </div>
  );
}

function App() {
  return (
    <form>
      <Name />
      <FavoriteAnimals />
      {/* STEP 1: Replace Person with the name entered in the Name component.*/}
      <div>Hey Person, your favorite animals are</div>

      {/* STEP 2: Replace the animals in the list with those entered in FavoriteAnimals. The value in FavoriteAnimals should be comma separated, so you need to parse it */}
      <ul>
        <li>Whale</li>
        <li>Cat</li>
      </ul>
    </form>
  );
}

export default App;
