// task 1: how the following code may be optimized?
async function completeCheckout(orderId) {
  await updateAnalytics(orderId);
  const order = await processOrder(orderId);

  return order;
}

// task 2: will catch block catch the error?
function eventuallyFail(time) {
  setTimeout(() => {
    throw Error("Oops!")
  }, time)
}

try {
  eventuallyFail(1000)
} catch (error) {
  console.error(error.message)
}

// task 3: there is a bug in the function. can you spot it ?
function calculateAverage(arr) {
  let sum = 0;
  let count = 0;

  for (let i = 0; i < arr.length; i++) {
    if (arr[i] && typeof arr[i] === "number") {
      sum += arr[i];
      count++;
    }
  }

  return sum / count;
}

// task 4: what will be logged to console?
function Welcome(name) {
  var greetingInfo = function (message) {
    console.log(message + " " + name);
  };
  return greetingInfo;
}

var myFunction = Welcome("John");

myFunction("Welcome ");
myFunction("Hello Mr.");
